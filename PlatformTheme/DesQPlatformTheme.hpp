/**
 * This file is a part of DesQ Theme.
 * DesQSession is the Session Manager for DesQ DE.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <private/qgenericunixthemes_p.h>
#include <qpa/qplatformtheme.h>
#include <QObject>
#include <QFont>
#include <QPalette>
#include <QLoggingCategory>
#include <QScopedPointer>
#include <QIcon>
#include <QFileInfo>

#include <DFSettings.hpp>

class QPalette;
class QPlatformSystemTrayIcon;
class QPlatformMenuBar;

class DesQPlatformTheme : public QObject, public QGenericUnixTheme {
    Q_OBJECT;

    public:
        DesQPlatformTheme();

        virtual ~DesQPlatformTheme();

        virtual bool usePlatformNativeDialog( DialogType type ) const override;
        virtual QPlatformDialogHelper *createPlatformDialogHelper( DialogType type ) const override;

        virtual const QPalette *palette( Palette type = SystemPalette ) const override;
#if QT_VERSION > QT_VERSION_CHECK( 6, 0, 0 )
        virtual Qt::ColorScheme colorScheme() const override;
#endif

        virtual const QFont *font( Font type          = SystemFont ) const override;

        virtual QVariant themeHint( ThemeHint hint ) const override;

        virtual QIcon fileIcon( const QFileInfo& fileInfo, QPlatformTheme::IconOptions iconOptions = {} ) const override;

        virtual QPlatformSystemTrayIcon *createPlatformSystemTrayIcon() const override;
        virtual QPlatformMenuBar * createPlatformMenuBar() const override;

    // QIconEngine *createIconEngine( const QString &iconName ) const override;

    private slots:
        void initSettings();
        void applySettings( QStringList );

    private:
        void loadSettings();
        void updateSetting( QString, QVariant );

        /**
         * Has objects from QtGui?
         * If yes, we will be sending palette change events to the application.
         */
        bool hasGui();
        bool hasWidgets();

        QString loadStyleSheets( const QStringList& paths );
        QPalette loadColorScheme( const QString& filePath );

        QString mStyle     = "onyx";
        QString mIconTheme = "breeze-dark";
        QString mUserStyleSheet;
        QString mPrevStyleSheet;
        QPalette mPalette;

        QFont mGeneralFont = *QGenericUnixTheme::font( QPlatformTheme::SystemFont );
        QFont mFixedFont   = *QGenericUnixTheme::font( QPlatformTheme::FixedFont );
        QFont mSmallFont   = *QGenericUnixTheme::font( QPlatformTheme::SystemFont );
        QFont mToolbarFont = *QGenericUnixTheme::font( QPlatformTheme::SystemFont );
        QFont mMenuFont    = *QGenericUnixTheme::font( QPlatformTheme::SystemFont );

        QStringList mIconPaths;

        QString mStandardDialogs     = "kde";
        bool mActivateOnSingleClick  = false;
        int mButtonsLayout           = 3;
        int mCursorFlashTime         = 1000;
        bool mDialogButtonsHaveIcons = true;
        int mDoubleClickInterval     = 400;
        int mGuiEffects           = 0;
        int mKeyboardScheme       = 3;
        bool mMenusHaveIcons      = true;
        bool mShowKeyBindsInMenus = true;
        int mToolButtonStyle      = 4;
        bool mShowMnemnonics      = true;
        int mWheelScrollLines     = 3;

        bool mUsePalette = true;

        mutable bool mDBusGlobalMenuAvailable = false;
        mutable bool mDBusTrayAvailable       = false;

        QStringList changedKeys;
};
