/**
 * This file is a part of DesQ Theme.
 * DesQSession is the Session Manager for DesQ DE.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QDir>
#include <QObject>
#include <QVariant>
#include <QStandardPaths>
#include <QDBusInterface>
#include <QGuiApplication>
#include <QScreen>
#include <QFont>
#include <QPalette>
#include <QTimer>
#include <QIcon>
#include <QRegularExpression>
#include <QMimeDatabase>
#include <QStyle>
#include <QStyleFactory>
#include <QApplication>
#include <QWidget>
#include <QFile>
#include <QString>
#include <QStringList>
#include <QFileSystemWatcher>
#include <QDBusConnectionInterface>

#include <private/qiconloader_p.h>
#include <private/qdbusmenubar_p.h>
#include <private/qdbustrayicon_p.h>
#include <qpa/qplatformthemefactory_p.h>
#include <private/qdbusmenuconnection_p.h>

#include <desq/desq-config.h>
#include <desq/Utils.hpp>
#include <DFXdg.hpp>
#include <DFSettings.hpp>
#include <DFColorScheme.hpp>

#include "DesQPlatformTheme.hpp"
#include "DesQSystemTrayIcon.hpp"

static inline bool matches( QString key, QStringList list ) {
    for ( QString item: list ) {
        if ( key.compare( item, Qt::CaseInsensitive ) == 0 ) {
            return true;
        }
    }

    return false;
}


static inline QFont getFontFromString( QVariant fontData ) {
    QStringList fontParts = fontData.toString().split( ", " );

    return QFont(
        fontParts.value( 0 ),
        fontParts.value( 1 ).toInt(),
        (fontParts.value( 2 ) == "true" ? QFont::Bold : QFont::Normal),
        (fontParts.value( 3 ) == "true" ? true : false)
    );
}


DesQPlatformTheme::DesQPlatformTheme() {
    /** Add DesQ theme path locations */
    DFL::Config::ColorScheme::addColorSchemePaths(
        {
            ThemePath "ColorSchemes",
            DFL::XDG::xdgDataHome() + "DesQ/Themes/ColorSchemes",
        }
    );

    if ( QFileInfo( QDir::home().filePath( ".icons" ) ).isDir() ) {
        mIconPaths << QDir::home().filePath( ".icons" );
    }

    mIconPaths << QStandardPaths::locateAll( QStandardPaths::GenericDataLocation, QStringLiteral( "icons" ), QStandardPaths::LocateDirectory );
    mIconPaths.removeDuplicates();

    /** Do not override application palette if it was set explicitly: ex. keepassxc */
    if ( QCoreApplication::testAttribute( Qt::AA_SetPalette ) ) {
        qCritical() << "Application refuses to use ColorSchemes of DesQ.";
        mUsePalette = false;
    }

    /**
     * Apply the settings at application startup
     * Also set a watch to determine what settings change
     */
    if ( QGuiApplication::desktopSettingsAware() ) {
        /**
         * Initialize our settings
         * This will
         *   1. create the DFL::Settings object,
         *   2. setup watcher to watch changes in the settings
         *   3. read the settings, and
         *   4. apply these settings.
         * All this will be done only if the app is desktopSettingsAware.
         */
        QMetaObject::invokeMethod( this, "initSettings", Qt::QueuedConnection );
    }
}


DesQPlatformTheme::~DesQPlatformTheme() {
}


bool DesQPlatformTheme::usePlatformNativeDialog( DialogType ) const {
    /** We will always use native dialogs. */
    return true;
}


QPlatformDialogHelper *DesQPlatformTheme::createPlatformDialogHelper( DialogType type ) const {
    QStringList keys = QPlatformThemeFactory::keys();
    QScopedPointer<QPlatformTheme> mTheme;

    if ( keys.contains( mStandardDialogs ) ) {
        mTheme.reset( QPlatformThemeFactory::create( mStandardDialogs ) );
    }

    /** Gtk2 is same as qt5gtk2 */
    else if ( (mStandardDialogs == QLatin1String( "gtk2" ) ) && keys.contains( "qt5gtk2" ) ) {
        mTheme.reset( QPlatformThemeFactory::create( "qt5gtk2" ) );
    }

    /** Gtk3 is same as qt5gtk3 */
    else if ( (mStandardDialogs == QLatin1String( "gtk3" ) ) && keys.contains( "qt5gtk3" ) ) {
        mTheme.reset( QPlatformThemeFactory::create( "qt5gtk3" ) );
    }

    return mTheme ? mTheme->createPlatformDialogHelper( type ) : QPlatformTheme::createPlatformDialogHelper( type );
}


const QPalette *DesQPlatformTheme::palette( QPlatformTheme::Palette ) const {
    return (mUsePalette) ? &mPalette : new QPalette( qApp->style()->standardPalette() );
}


const QFont *DesQPlatformTheme::font( QPlatformTheme::Font type ) const {
    switch ( type ) {
        case QPlatformTheme::SystemFont: {
            return &mGeneralFont;
        }

        case QPlatformTheme::MenuFont: {
            return &mMenuFont;
        }

        case QPlatformTheme::MenuBarFont: {
            return &mMenuFont;
        }

        case QPlatformTheme::MenuItemFont: {
            return &mMenuFont;
        }

        case QPlatformTheme::MessageBoxFont: {
            return &mGeneralFont;
        }

        case QPlatformTheme::LabelFont: {
            return &mGeneralFont;
        }

        case QPlatformTheme::TipLabelFont: {
            return &mSmallFont;
        }

        case QPlatformTheme::StatusBarFont: {
            return &mGeneralFont;
        }

        case QPlatformTheme::TitleBarFont: {
            return &mGeneralFont;
        }

        case QPlatformTheme::MdiSubWindowTitleFont: {
            return &mGeneralFont;
        }

        case QPlatformTheme::DockWidgetTitleFont: {
            return &mGeneralFont;
        }

        case QPlatformTheme::PushButtonFont: {
            return &mGeneralFont;
        }

        case QPlatformTheme::CheckBoxFont: {
            return &mGeneralFont;
        }

        case QPlatformTheme::RadioButtonFont: {
            return &mGeneralFont;
        }

        case QPlatformTheme::ToolButtonFont: {
            return &mToolbarFont;
        }

        case QPlatformTheme::ItemViewFont: {
            return &mGeneralFont;
        }

        case QPlatformTheme::ListViewFont: {
            return &mGeneralFont;
        }

        case QPlatformTheme::HeaderViewFont: {
            return &mGeneralFont;
        }

        case QPlatformTheme::ListBoxFont: {
            return &mGeneralFont;
        }

        case QPlatformTheme::ComboMenuItemFont: {
            return &mGeneralFont;
        }

        case QPlatformTheme::ComboLineEditFont: {
            return &mGeneralFont;
        }

        case QPlatformTheme::SmallFont: {
            return &mSmallFont;
        }

        case QPlatformTheme::MiniFont: {
            return &mSmallFont;
        }

        case QPlatformTheme::FixedFont: {
            return &mFixedFont;
        }

        case QPlatformTheme::GroupBoxTitleFont: {
            return &mGeneralFont;
        }

        case QPlatformTheme::TabButtonFont: {
            return &mToolbarFont;
        }

        case QPlatformTheme::EditorFont: {
            return &mFixedFont;
        }

        default: {
            return &mGeneralFont;
        }
    }

    return &mGeneralFont;
}


QVariant DesQPlatformTheme::themeHint( QPlatformTheme::ThemeHint hint ) const {
    switch ( hint ) {
        case QPlatformTheme::CursorFlashTime: {
            return mCursorFlashTime;
        }

        case MouseDoubleClickInterval: {
            return mDoubleClickInterval;
        }

        case QPlatformTheme::ToolButtonStyle: {
            return mToolButtonStyle;
        }

        case QPlatformTheme::SystemIconThemeName: {
            return mIconTheme;
        }

        case QPlatformTheme::StyleNames: {
            return QStringList() << mStyle << mStyle.toLower();
        }

        case QPlatformTheme::IconThemeSearchPaths: {
            return mIconPaths;
        }

        case QPlatformTheme::DialogButtonBoxLayout: {
            return mButtonsLayout;
        }

        case QPlatformTheme::KeyboardScheme: {
            return mKeyboardScheme;
        }

        /** Disabled */
        case QPlatformTheme::UiEffects: {
            return 0;
        }

        case QPlatformTheme::WheelScrollLines: {
            return mWheelScrollLines;
        }

        case QPlatformTheme::ShowShortcutsInContextMenus: {
            return mShowKeyBindsInMenus;
        }

        default: {
            return QPlatformTheme::themeHint( hint );
        }
    }
}

#if QT_VERSION > QT_VERSION_CHECK( 6, 0, 0 )
Qt::ColorScheme DesQPlatformTheme::colorScheme() const {
    return Qt::ColorScheme::Dark;
}
#endif

QIcon DesQPlatformTheme::fileIcon( const QFileInfo& fileInfo, QPlatformTheme::IconOptions iconOptions ) const {
    if ( fileInfo.isDir() ) {
        /** Normal directories */
        if ( iconOptions & DontUseCustomDirectoryIcons ) {
            return QIcon::fromTheme( QLatin1String( "inode-directory" ) );
        }

        /** Special folders for pictures, videos, etc */
        else {
            QString dirName( fileInfo.fileName() );
            QString dirPath( fileInfo.filePath() );

            if ( matches( dirName, { "pic", "img", "image", "picture", "wallpaper", "wall paper", "background" } ) ) {
                return QIcon::fromTheme( "folder-images" );
            }

            else if ( matches( dirName, { "vid", "video", "movie" } ) ) {
                return QIcon::fromTheme( "folder-videos" );
            }

            else if ( matches( dirName, { "music", "sound", "audio", "recording", "snd" } ) ) {
                return QIcon::fromTheme( "folder-sound" );
            }

            else {
                QSettings dirSett( dirPath + "/.directory", QSettings::IniFormat );
                QString   iconName = dirSett.value( "Desktop Entry/Icon", "folder" ).toString();

                return QIcon::fromTheme( iconName, QIcon( iconName ) );
            }
        }
    }

    QMimeDatabase db;
    QMimeType     type = db.mimeTypeForFile( fileInfo );

    return QIcon::fromTheme( type.iconName() );
}


QPlatformMenuBar *DesQPlatformTheme::createPlatformMenuBar() const {
    return nullptr;
}


QPlatformSystemTrayIcon *DesQPlatformTheme::createPlatformSystemTrayIcon() const {
    return new DesQSystemTrayIcon();
}


void DesQPlatformTheme::initSettings() {
    DFL::Settings *settings = DesQ::Utils::initializeDesQSettings( "Looks", "Looks", true );

    QStringList allKeys = settings->allKeys();

    for ( QString key: allKeys ) {
        updateSetting( key, settings->value( key ) );
    }

    applySettings( allKeys );

    QTimer *timer = new QTimer( this );

    timer->setSingleShot( true );
    timer->setInterval( 5000 );

    /** Connect the setting changed notification to the timer with 5s timeout */
    connect(
        settings, &DFL::Settings::settingChanged, [ = ]( QString key, QVariant value ) mutable {
            /**
             * Update the setting immediately, and remember the changed key
             * Apply the setting after a delay of 5s <- Should we change this?
             */
            updateSetting( key, value );
            changedKeys << key;

            /**
             * All changes made within 1s of the first change will be loaded.
             * This will avoid multiple calls to the applySettings(...) function.
             */
            if ( timer->isActive() == false ) {
                timer->start();
            }
        }
    );

    /** Update the settings when the timer goes out */
    connect(
        timer, &QTimer::timeout, [ = ]() mutable {
            applySettings( changedKeys );
            changedKeys.clear();
        }
    );
}


void DesQPlatformTheme::updateSetting( QString key, QVariant value ) {
    /** Widget Style */
    if ( key == "Appearance::Styles::WidgetStyle" ) {
        mStyle = value.toString();
    }

    /** Color Scheme */
    else if ( key == "Appearance::Colors::Scheme" ) {
        QString colorScheme = value.toString();
        mPalette = loadColorScheme( colorScheme );
    }

    /** Icon Theme */
    else if ( key == "Appearance::Icons::Theme" ) {
        mIconTheme = value.toString();
    }

    /** Fixed Font */
    else if ( key == "Fonts::AppFonts::Fixed" ) {
        mFixedFont = getFontFromString( value );
    }

    /** General Font */
    else if ( key == "Fonts::AppFonts::General" ) {
        mGeneralFont = getFontFromString( value );
    }

    /** Menu Font */
    else if ( key == "Fonts::AppFonts::Menu" ) {
        mMenuFont = getFontFromString( value );
    }

    /** Small Font */
    else if ( key == "Fonts::AppFonts::Small" ) {
        mSmallFont = getFontFromString( value );
    }

    /** Toolbar Font */
    else if ( key == "Fonts::AppFonts::Toolbar" ) {
        mToolbarFont = getFontFromString( value );
    }

    /** Native Dialog Style */
    else if ( key == "Interface::Dialogs::StandardDialogs" ) {
        mStandardDialogs = value.toString();
    }

    else if ( key == "Interface::Effects::ActivateOnSingleClick" ) {
        mActivateOnSingleClick = value.toBool();
    }

    else if ( key == "Interface::Dialogs::ButtonsLayout" ) {
        mButtonsLayout = value.toInt();
    }

    else if ( key == "Interface::Effects::CursorFlashTime" ) {
        mCursorFlashTime = value.toInt();
    }

    else if ( key == "Interface::Dialogs::ButtonsHaveIcons" ) {
        mDialogButtonsHaveIcons = value.toBool();
    }

    else if ( key == "Interface::Effects::DoubleClickInterval" ) {
        mDoubleClickInterval = value.toInt();
    }

    else if ( key == "Interface::Effects::GuiEffects" ) {
        mGuiEffects = value.toBool();
    }

    else if ( key == "Interface::UiElements::KeyboardScheme" ) {
        mKeyboardScheme = value.toInt();
    }

    else if ( key == "Interface::UiElements::MenusHaveIcons" ) {
        mMenusHaveIcons = value.toBool();
    }

    else if ( key == "Interface::UiElements::ShowKeyBindsInMenus" ) {
        mShowKeyBindsInMenus = value.toBool();
    }

    else if ( key == "Interface::UiElements::ToolButtonStyle" ) {
        mToolButtonStyle = value.toInt();
    }

    else if ( key == "Interface::UiElements::ShowMnemnonics" ) {
        mShowMnemnonics = value.toBool();
    }

    else if ( key == "Interface::UiElements::WheelScrollLines" ) {
        mWheelScrollLines = value.toInt();
    }
}


bool DesQPlatformTheme::hasGui() {
    return qobject_cast<QGuiApplication *>( qApp ) != nullptr;
}


bool DesQPlatformTheme::hasWidgets() {
    return qobject_cast<QApplication *>( qApp ) != nullptr;
}


QString DesQPlatformTheme::loadStyleSheets( const QStringList& paths ) {
    QString content;

    for ( const QString& path : paths ) {
        if ( !QFile::exists( path ) ) {
            continue;
        }

        QFile file( path );
        file.open( QIODevice::ReadOnly );
        content.append( QString::fromUtf8( file.readAll() ) );
    }
    QRegularExpression regExp( "//.*( \\n|$ )" );

    content.remove( regExp );
    return content;
}


QPalette DesQPlatformTheme::loadColorScheme( const QString& colors ) {
    DFL::Config::ColorScheme scheme( colors );

    return scheme.palette();
}


void DesQPlatformTheme::applySettings( QStringList keys ) {
    for ( QString key: keys ) {
        /** Widget Style: Re-apply DesQProxyStyle */
        if ( key == "Appearance::Styles::WidgetStyle" ) {
            if ( QApplication::allWidgets().count() ) {
                qApp->setStyle( "desq" );
            }
        }

        /** Color Scheme */
        else if ( key == "Appearance::Colors::Scheme" ) {
            if ( mUsePalette ) {
                qApp->setPalette( mPalette );
            }
        }

        /** Icon Theme */
        else if ( key == "Appearance::Icons::Theme" ) {
            /**
             * QPlatformTheme::themeHint( QPlatformTheme::SystemIconThemeName )
             * will return the newly set icon theme. It will be used by QIconLoader
             */
            QIconLoader::instance()->setThemeName( mIconTheme );
            QIconLoader::instance()->updateSystemTheme();
        }

        /** Fixed Font */
        else if ( key == "Fonts::AppFonts::Fixed" ) {
            // Handled by QPlatformTheme::font( QPlatformTheme::SystemFont )
        }

        /** General Font */
        else if ( key == "Fonts::AppFonts::General" ) {
            QGuiApplication::setFont( mGeneralFont );
        }

        /** Menu Font */
        else if ( key == "Fonts::AppFonts::Menu" ) {
            // Handled by QPlatformTheme::font( QPlatformTheme::SystemFont )
        }

        /** Small Font */
        else if ( key == "Fonts::AppFonts::Small" ) {
            // Handled by QPlatformTheme::font( QPlatformTheme::SystemFont )
        }

        /** Toolbar Font */
        else if ( key == "Fonts::AppFonts::Toolbar" ) {
            // Handled by QPlatformTheme::font( QPlatformTheme::SystemFont )
        }

        /** Native Dialog Style */
        else if ( key == "Interface::Dialogs::StandardDialogs" ) {
            QStringList keys = QPlatformThemeFactory::keys();
            QScopedPointer<QPlatformTheme> mTheme;

            if ( keys.contains( mStandardDialogs ) ) {
                mTheme.reset( QPlatformThemeFactory::create( mStandardDialogs ) );
            }

            /** Gtk2 is same as qt5gtk2 */
            else if ( (mStandardDialogs == QLatin1String( "gtk2" ) ) && keys.contains( "qt5gtk2" ) ) {
                mTheme.reset( QPlatformThemeFactory::create( "qt5gtk2" ) );
            }

            /** Gtk3 is same as qt5gtk3 */
            else if ( (mStandardDialogs == QLatin1String( "gtk3" ) ) && keys.contains( "qt5gtk3" ) ) {
                mTheme.reset( QPlatformThemeFactory::create( "qt5gtk3" ) );
            }
        }

        else if ( key == "Interface::Effects::ActivateOnSingleClick" ) {
            /** Handled by DesQ Proxy Style: Reinitialize the style */
            if ( QApplication::allWidgets().count() ) {
                qApp->setStyle( "desq" );
            }
        }

        else if ( key == "Interface::Dialogs::ButtonsLayout" ) {
            /**
             * Handled by
             * QPlatformTheme::themeHint( QPlatformTheme::DialogButtonBoxLayout )
             * Changing the setting will not change the existing layout
             */
        }

        else if ( key == "Interface::Effects::CursorFlashTime" ) {
            /**
             * Handled by
             * QPlatformTheme::themeHint( QPlatformTheme::CursorFlashTime )
             * Changing the setting will not change the existing layout
             */
        }

        else if ( key == "Interface::Dialogs::ButtonsHaveIcons" ) {
            /** Handled by DesQ Proxy Style: Reinitialize the style */
            if ( QApplication::allWidgets().count() ) {
                qApp->setStyle( "desq" );
            }
        }

        else if ( key == "Interface::Effects::DoubleClickInterval" ) {
            /**
             * Handled by
             * QPlatformTheme::themeHint( QPlatformTheme::DoubleClickInterval )
             * Changing the setting will not change the existing layout
             */
        }

        else if ( key == "Interface::Effects::GuiEffects" ) {
            /**
             * Handled by
             * QPlatformTheme::themeHint( QPlatformTheme::GuiEffects )
             * Changing the setting will not change the existing layout
             */
        }

        else if ( key == "Interface::UiElements::KeyboardScheme" ) {
            /**
             * Handled by
             * QPlatformTheme::themeHint( QPlatformTheme::KeyboardScheme )
             * Changing the setting will not change the existing layout
             */
        }

        else if ( key == "Interface::UiElements::MenusHaveIcons" ) {
            QCoreApplication::setAttribute( Qt::AA_DontShowIconsInMenus, not mMenusHaveIcons );
        }

        else if ( key == "Interface::UiElements::ShowKeyBindsInMenus" ) {
            /**
             * Handled by
             * QPlatformTheme::themeHint( QPlatformTheme::ShowShortcutsInContextMenus )
             * Changing the setting will not change the existing layout
             */
        }

        else if ( key == "Interface::UiElements::ToolButtonStyle" ) {
            /**
             * Handled by
             * QPlatformTheme::themeHint( QPlatformTheme::ToolButtonStyle )
             * Changing the setting will not change the existing layout
             */
        }

        else if ( key == "Interface::UiElements::ShowMnemnonics" ) {
            /** Handled by DesQ Proxy Style: Reinitialize the style */
            if ( QApplication::allWidgets().count() ) {
                qApp->setStyle( "desq" );
            }
        }

        else if ( key == "Interface::UiElements::WheelScrollLines" ) {
            qApp->setWheelScrollLines( mWheelScrollLines );
        }
    }

    if ( hasGui() ) {
        QEvent e( QEvent::PaletteChange );
        QApplication::sendEvent( qApp, &e );
    }

    if ( hasWidgets() ) {
        for ( QWidget *w : QApplication::allWidgets() ) {
            QEvent e( QEvent::ThemeChange );
            QApplication::sendEvent( w, &e );
        }

        for ( QWidget *w : qApp->allWidgets() ) {
            QEvent e( QEvent::StyleChange );
            QApplication::sendEvent( w, &e );
        }
    }
}
