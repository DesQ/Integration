# DesQ Themes
DesQ Themes provieds two qt5 plugins: DesQ Platform Theme plugin, and DesQ Style plugin.
These plugins, along with env variable `QT_QPA_PLATFORMTHEME=desq`, allows us to control
Qt theming in DesQ.

Currently, it also provides a simple binary `desq-theme-settings` to choose the Qt
application themes.


### Dependencies:
* Qt5 (qtbase5-dev, qtbase5-dev-tools, qtbase5-private-dev )
* LibDesQ (https://gitlab.com/DesQ/libdesq.git)


### Notes for compiling (Qt5) - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/DesQ/Theme.git DesQTheme`
- Enter the `DesQTheme` folder
  * `cd DesQTheme`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Known Bugs
* DesQ Theme color scheme loading is faulty.


### Upcoming
* Any feature you ask for... :)
