/**
 * The GPL v3 License
 *
 * Copyright (c) 2023 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2023 Victor Tran (https://github.com/vicr123)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QtWaylandClient/private/qwaylandshellintegration_p.h>

namespace WQt {
    class LayerShell;
}

class WlrLayerShellIntegration : public QtWaylandClient::QWaylandShellIntegration {
    public:
        explicit WlrLayerShellIntegration();

    signals:

    // QWaylandShellIntegration interface
    public:
        bool initialize( QtWaylandClient::QWaylandDisplay *display );

        const struct wl_interface *extensionInterface() const;
        void bind( struct ::wl_registry *registry, int id, int ver );
        QtWaylandClient::QWaylandShellSurface * createShellSurface( QtWaylandClient::QWaylandWindow *window );

    private:
        WQt::LayerShell *mLayerShell        = nullptr;
        QWaylandShellIntegration *mXdgShell = nullptr;

        bool shouldBeLayerShell( QtWaylandClient::QWaylandWindow *window );
};
